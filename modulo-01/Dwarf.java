
/**
 * Escreva a descrição da classe Dwarf aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class Dwarf
{
    private String nome;
    private float vida = 110;
    
    public Dwarf(String nome){
        this.nome = nome;
    }
    
    public float getVida(){
        return this.vida;
    }
    
    public void decrementarVida(){
        this.vida+= -10;
    } 
}
