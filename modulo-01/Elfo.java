public class Elfo
{
    private String nome;
    private Item flecha = new Item(4, "Flecha");    
    private Item arco = new Item(1, "Arco");
    private int exp = 0;
    
    public Elfo(String nome){
        this.nome = nome;
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setNome(String nome){
     this.nome = nome;   
    }
    
    public int getExp(){
        return exp;
    }
    
    public void incrementarExp(int exp){
        this.exp = this.exp + exp;
    }
    
    private Item getFlecha(){
        return this.flecha;
    }
    
    public int getQntFlecha(){
        return this.getFlecha().getQuantidade();   
    }
    
    private int quantidadeItem(Item item){
        return item.getQuantidade();
    }
    
    private void incrimentarFlecha(int quantidade){
     getFlecha().acrescentarQuantidade(quantidade);  
    } 
    
    public void atirarFlecha(Dwarf dwarf){
     int qntAtual = this.getFlecha().getQuantidade();
     
        if(qntAtual>0 && dwarf.getVida()>= 10){
            flecha.setQuantidade(qntAtual - 1);
            this.incrementarExp(1);
            dwarf.decrementarVida();
        }
        else
             System.out.println("Não há mais flechas ou o dwarf morreu!");
            
    }
}
